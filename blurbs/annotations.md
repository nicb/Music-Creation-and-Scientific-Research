# Musica e Ricerca - annotazioni

## Titolo dell'intervento 22/11/2024: *Musica e Ricerca: da Leonino a Luigi Nono*

* La ricerca è sempre esistita in musica, da quando questa attività ha questo nome
* L'attività del *cercare* in musica si chiama *comporre*
* Comporre significa(va) *cercare* il modo, le tecnologie e le notazioni più appropriate,
  in un dato momento storico e in un dato contesto, per organizzare suoni in
  modo da comunicare qualcosa (esempi dal '300 al '900)

* Per questo, la musica ha sempre avuto uno stretto rapporto con la ricerca
  tecnologica e, in tempi più recenti, con la ricerca scientifica:
  le scoperte da quest'ultimo lato sono spesso state trasferite a specifiche
  "visioni musicali" (spettralismo, musica elettronica)
* questo modo di vedere le cose dà per scontata una visione *speculativa*
  della composizione musicale. Oggi, la stragrande maggioranza della musica
  viene creata e prodotta esclusivamente a fini d'intrattenimento: non c'è più
  bisogno di ricerca. E anche la tecnologia si è fermata.
* il problema dell'ascolto: per continuare a cercare in musica, bisogna avere
  ascoltatori in grado di ascoltare (il caso del *Lexicon of Musical
  Invective* di Slonimsky)
* Due esempi: Nono e Razzi
* considerazioni sul futuro
